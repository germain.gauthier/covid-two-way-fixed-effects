# This script contains all relevant functions used to simulate data from a SIRD data-generating process.

library(reshape2)
library(dplyr)

# Parameters

# gamma = inverse of infection time
# mu = death rate
# lambda = parameter of the exponential decay process
# beta = baseline infection rate
# effect = effect of the mitigation policy on the infection rate
# effect_confounder = effect of a binary, time-varying confounder on the infection rate
# I_0 = size of the initially infected population
# a = time-trend on the infection rate
# noise = add a poisson distribution process to the flow of new infected individuals

# This function simulates a SIRD model for a given set of parameters.
simulate_dgp <- function(nreps, 
                         pop, 
                         beta, 
                         gamma, 
                         mu, 
                         fe,
                         thresh,
                         effect,
                         effect_confounder = 0,
                         I_0,
                         a = 0,
                         noise = F){
  
  I = I_0
  R = 0
  D = 0
  newInfected = I_0
  newDead = 0
  
  S = pop - I - R - D
  N = pop
  
  beta_t = 0
  
  X = round(runif(nreps))
  
  sim = data.frame(time=0, S, I, R, D, newInfected, newDead, N, beta_t, trt = 0, X = 0)
  
  for(time in 1:nreps){
    
      if (time < thresh){
        trt = 0
      } else {
        trt = 1
      }
      
      beta_t = beta * exp(a*time + fe + effect*trt + effect_confounder*X[time]) 
      
      if (noise == F){
        newInfected = floor(beta_t*I*S/N)
      } else {
        newInfected = rpois(1, floor(beta_t*I*S/N))
      }
      
      newRecovered = floor((1-mu)*gamma*I)
      newDead = floor(gamma*mu*I)
      
      S = max(S - newInfected - newDead,0)
      I = max(I + newInfected - newRecovered,0)
      R = R + newRecovered
      D = D + newDead
      
      sim = rbind(sim, data.frame(time, S, I, R, D, newInfected, newDead, N, beta_t, trt = trt, X = X[time]))
    
  }
  
  sim$time_treated = thresh
  
  return(sim)
  
}

# This function simulates epidemic trajectories for multiple geographical units.
make_df <- function(ngroups, 
                    nreps, 
                    pop, 
                    beta = 0.3, 
                    gamma = 0.1, 
                    mu = 0.01, 
                    fe,
                    thresh,
                    effect = 0,
                    effect_confounder = 0,
                    I_0,
                    a,
                    noise = F){
  
  for (i in c(1:ngroups)){
    
    if (i == 1){
      
      df <- simulate_dgp(nreps = nreps, 
                         pop = pop[i], 
                         beta = beta, 
                         gamma = gamma, 
                         mu = mu, 
                         fe = fe[i],
                         thresh = thresh[i],
                         effect = effect,
                         effect_confounder = effect_confounder,
                         I_0 = I_0[i],
                         a = 0,
                         noise = noise)
      
      df$dep <- i
      
    } else {
      
      temp <- simulate_dgp(nreps = nreps, 
                         pop = pop[i], 
                         beta = beta, 
                         gamma = gamma, 
                         mu = mu, 
                         fe = fe[i],
                         thresh = thresh[i],
                         effect = effect,
                         effect_confounder = effect_confounder,
                         I_0 = I_0[i],
                         a = 0,
                         noise = noise)
      
      temp$dep <- i
      
      df <- rbind(df, temp)
      
    }
  }
  
  df <- df %>% filter(time != 0)
  
  df$dep <- as.character(df$dep)
  
  df <- df %>% group_by(dep) %>% mutate(ln_NI = log(newInfected),
                                        delta_ln_NI = ln_NI -dplyr::lag(ln_NI),
                                        cumsum_NI = cumsum(newInfected)/N)
  
  df <- df %>% group_by(dep) %>% mutate(treated = case_when(max(trt) == 1 ~ 1, TRUE ~ 0))
  
  return(df)
}

