# This script simulates epidemic trajectories assuming a (stochastic) SIRD data-generating process 
# and estimates different specifications discussed in the paper.

# NB: The interested reader will notice that the simulation functions allows for additional options
# than the ones used in the paper (e.g., linear time trends, binary time-varying confounders, stochastic noise)

require(ggplot2)
library(dplyr)
library(fixest)
library(ggpubr)
source('simulation_functions.R')

set.seed(123)

# Simulations
#...............................................................................
#...............................................................................

# Keep it even if you don't want the code to break down. Yes, I'm lazy!
ngroups = 100

# Simulate data for X periods:
nreps = 150

# Infection rate:
beta = 0.12

# Death rate:
mu = 0.01

# Recovery rate:
gamma = 0.1

# Treatment takes place at date:
treatment_date = 25

# Treatment decreases the infection rate by: (0 or -0.1 in the draft)
effect = my_effect

# Binary, time-varying confounder effect on infection rate:
effect_confounder = 0

# Time-trend on infection rate:
a = 0

# Noise in the simulations:
noise = F

# size of window for the event study
w <- 100

# Parallel trends don't hold .
# Regions have baseline unobserved heterogeneity in infection rates.
#...............................................................................

# Size of the populations
pop = runif(ngroups, 100000, 1000000) 

# Initial size of the infected population
I_0 = floor(runif(ngroups, 0.001, 0.01) * pop)
# I_0 = floor(c(rep(0.01, ngroups/2), rep(0.001, ngroups/2))*pop)

# Unobserved heterogeneity in infection rates.
fe <- runif(ngroups,0,0.5)
# fe = c(rep(0.05, ngroups/2), rep(0, ngroups/2))

# Policy is implemented for half of the observations.
thresh = c(floor(runif(ngroups/2, 1, nreps)),rep(10000, ngroups/2))
# thresh = c(rep(treatment_date, ngroups/2), rep(100000, ngroups/2))

# Create dataset
df <- make_df(ngroups = ngroups, 
              nreps = nreps, 
              pop = pop, 
              beta = beta, 
              gamma = gamma, 
              mu = mu, 
              fe = fe, 
              thresh = thresh,
              effect = effect,
              effect_confounder = effect_confounder,
              I_0 = I_0, 
              a = a,
              noise = noise)

df_inefficient <- make_df(ngroups = ngroups, 
                          nreps = nreps, 
                          pop = pop, 
                          beta = beta, 
                          gamma = gamma, 
                          mu = mu, 
                          fe = fe, 
                          thresh = thresh,
                          effect = 0,
                          effect_confounder = effect_confounder,
                          I_0 = I_0, 
                          a = a,
                          noise = noise)

ggplot(df) +
  aes(x = time, y = newInfected/N, group = dep, color = dep) +
  geom_line() +
  xlab('') +
  ylab('') +
  scale_color_grey() +
  theme_bw() +
  theme(legend.position = "none")

g1 <- ggplot(df) +
  aes(x = time, y = cumsum_NI, group = dep, color = dep) +
  geom_line() +
  xlab('') +
  ylab('') +
  scale_color_grey() +
  theme_bw() +
  theme(legend.position = "none")

g2 <- ggplot(df) +
  aes(x = time, y = ln_NI, group = dep, color = dep) +
  geom_line() +
  xlab('') +
  ylab('') +
  scale_color_grey() +
  theme_bw() +
  theme(legend.position = "none")

g3 <- ggplot(df) +
  aes(x = time, y = delta_ln_NI, group = dep, color = dep) +
  geom_line() +
  xlab('') +
  ylab('') +
  scale_color_grey() +
  theme_bw() +
  theme(legend.position = "none")


my_size = 7
col1 <- ggplot() + annotate(geom = 'text', x=1, y=1, label="Cumulative", size = my_size) + theme_void()
col2 <- ggplot() + annotate(geom = 'text', x=1, y=1, label="Logs", size = my_size) + theme_void()
col3 <- ggplot() + annotate(geom = 'text', x=1, y=1, label="Delta Logs", size = my_size) + theme_void()


ggarrange(col1, col2, col3, g1, g2, g3, 
          ncol = 3, 
          nrow = 2, 
          align = 'v', 
          common.legend = T, 
          legend = 'none',
          widths = c(1,1),
          heights = c(1,5)) 

ggsave(paste0('../graphs/simulations_', effect, '.pdf'), width = 25, height = 10, units = "cm")


# CUMSUM
twfe_constant <- feols(cumsum_NI ~ trt | time + dep, data = df)
summary(twfe_constant)

df_inefficient$cumsum_NI_hat <- predict(twfe_constant, newdata = df_inefficient)

se <- df_inefficient %>% 
  group_by(dep) %>%
  summarize(true_y_0 = sum(newInfected),
            predicted_y_0 = max(cumsum_NI_hat)*mean(N),
            se = (true_y_0 - predicted_y_0)^2)

rmse <- sqrt(mean(se$se))
se <- twfe_constant$se['trt']
trt <- twfe_constant$coefficients['trt']

sim = data.frame(policy = effect, specification = "DID", Y = "Cumulative",  tau = trt, se = se, rmse = rmse)

# LOGS
twfe_constant <- feols(ln_NI ~ trt | time + dep, data = df)
summary(twfe_constant)

df_inefficient$ln_NI_hat <- predict(twfe_constant, newdata = df_inefficient)

se <- df_inefficient %>% 
  group_by(dep) %>%
  summarize(true_y_0 = sum(newInfected),
            predicted_y_0 = sum(exp(ln_NI_hat)),
            se = (true_y_0 - predicted_y_0)^2)

rmse <- sqrt(mean(se$se))
se <- twfe_constant$se['trt']
trt <- twfe_constant$coefficients['trt']

sim = rbind(sim, data.frame(policy = effect, specification = "DID", Y = "Log",  tau = trt, se = se, rmse = rmse))

# DELTA LOGS
twfe_constant <- feols(delta_ln_NI ~ trt | time + dep, data = df)
summary(twfe_constant)

df_inefficient$delta_ln_NI_hat <- predict(twfe_constant, newdata = df_inefficient)

compute_cumsum_NI_hat <- function(i,k){
  NI_ref <- df$newInfected[df$dep == i & df$time == (k-1)]
  
  df2 <- df_inefficient %>% filter(as.numeric(time)>=k & dep == i)  
  
  compute_ni <- function(time){
    return(NI_ref*exp(sum(df2$delta_ln_NI_hat[df2$time <= time])))
  }
  
  NI_hat <- sum(unlist(lapply(df_inefficient$time[df_inefficient$dep == i], compute_ni)))
  
  return(NI_hat)
  
}

se <- df_inefficient %>% 
  group_by(dep) %>%
  summarize(N = mean(N),
            cumsum_NI_hat = compute_cumsum_NI_hat(dep, 2),
            cumsum_NI = sum(newInfected),
            se = (cumsum_NI - cumsum_NI_hat)^2)


rmse <- sqrt(mean(se$se))
se <- twfe_constant$se['trt']
trt <- twfe_constant$coefficients['trt']

sim = rbind(sim, data.frame(policy = effect, specification = "DID", Y = "Delta Log",  tau = trt, se = se, rmse = rmse))


# 2FE model with dynamic treatment effects ("event study") 

df <- df %>% group_by(dep) %>%
  mutate(time_to_treatment = case_when(
    time - time_treated > -w & time - time_treated < w ~ time - time_treated,
    time - time_treated <= -w ~ -w,
    time - time_treated >= w ~ w))

df_inefficient <- df_inefficient %>% group_by(dep) %>%
  mutate(time_to_treatment = case_when(
    time - time_treated > -w & time - time_treated < w ~ time - time_treated,
    time - time_treated <= -w ~ -w,
    time - time_treated >= w ~ w))

df_inefficient$treated <- 0 # for counterfactuals

# CUMSUM
twfe_dynamic <- feols(cumsum_NI ~ i(treated, time_to_treatment, ref = -1) | time + dep, data = df, se = 'twoway')

df_inefficient$cumsum_NI_hat <- predict(twfe_dynamic, newdata = df_inefficient)

se <- df_inefficient %>% 
  group_by(dep) %>%
  summarize(true_y_0 = sum(newInfected),
            predicted_y_0 = max(cumsum_NI_hat)*mean(N),
            se = (true_y_0 - predicted_y_0)^2)

rmse <- sqrt(mean(se$se))
se <- twfe_dynamic$se['treated:time_to_treatment::0']
trt <- twfe_dynamic$coefficients['treated:time_to_treatment::0']

sim = rbind(sim, data.frame(policy = effect, specification = "Event Study", Y = "Cumulative",  tau = trt, se = se, rmse = rmse))

g1 <- coefplot(twfe_dynamic)

# LOGS
twfe_dynamic <- feols(ln_NI ~i(treated, time_to_treatment, ref = -1) | time + dep, data = df, se = 'twoway')

df_inefficient$ln_NI_hat <- predict(twfe_dynamic, newdata = df_inefficient)

se <- df_inefficient %>% 
  group_by(dep) %>%
  summarize(true_y_0 = sum(newInfected),
            predicted_y_0 = sum(exp(ln_NI_hat)),
            se = (true_y_0 - predicted_y_0)^2)

rmse <- sqrt(mean(se$se))
se <- twfe_dynamic$se['treated:time_to_treatment::0']
trt <- twfe_dynamic$coefficients['treated:time_to_treatment::0']

sim = rbind(sim, data.frame(policy = effect, specification = "Event Study", Y = "Log",  tau = trt, se = se, rmse = rmse))

g2 <- coefplot(twfe_dynamic)

# DELTA LOGS
twfe_dynamic <- feols(delta_ln_NI ~ i(treated, time_to_treatment, ref = -1) | time + dep, data = df, se = 'twoway')

df_inefficient$delta_ln_NI_hat <- predict(twfe_dynamic, newdata = df_inefficient)

se <- df_inefficient %>% 
  group_by(dep) %>%
  summarize(N = mean(N),
            cumsum_NI_hat = compute_cumsum_NI_hat(dep, 2),
            cumsum_NI = sum(newInfected),
            se = (cumsum_NI - cumsum_NI_hat)^2)


rmse <- sqrt(mean(se$se))
se <- twfe_dynamic$se['treated:time_to_treatment::0']
trt <- twfe_dynamic$coefficients['treated:time_to_treatment::0']

sim = rbind(sim, data.frame(policy = effect, specification = "Event Study", Y = "Delta Log", tau = trt, se = se, rmse = rmse))

g3 <- coefplot(twfe_dynamic)

g1 <- ggplot(g1$prms) +
  aes(x = x, y = y) +
  geom_line() +
  geom_ribbon(aes(ymin = ci_low, ymax =  ci_high), alpha = 0.3) +
  geom_vline(xintercept = -1, linetype = 'solid') +
  geom_hline(yintercept = 0, linetype = 'longdash') +
  xlab('Time to Treatment') +
  ylab('Estimate (95% CI)') +
  theme_bw()

g2 <- ggplot(g2$prms) +
  aes(x = x, y = y) +
  geom_line() +
  geom_ribbon(aes(ymin = ci_low, ymax =  ci_high), alpha = 0.3) +
  geom_vline(xintercept = -1, linetype = 'solid') +
  geom_hline(yintercept = 0, linetype = 'longdash') +
  xlab('Time to Treatment') +
  ylab('') +
  theme_bw()

g3 <- ggplot(g3$prms) +
  aes(x = x, y = y) +
  geom_line() +
  geom_ribbon(aes(ymin = ci_low, ymax =  ci_high), alpha = 0.3) +
  geom_vline(xintercept = -1, linetype = 'solid') +
  geom_hline(yintercept = 0, linetype = 'longdash') +
  xlab('Time to Treatment') +
  ylab('') +
  theme_bw()

ggarrange(col1, col2, col3, g1, g2, g3, 
          ncol = 3, 
          nrow = 2, 
          align = 'v', 
          common.legend = T, 
          legend = 'none',
          widths = c(1,1),
          heights = c(1,5)) 

ggsave(paste0('../graphs/event_study_', effect, '.pdf'), width = 25, height = 10, units = "cm")

library(xtable)
print(xtable(sim, digits = 4), include.rownames=FALSE)
