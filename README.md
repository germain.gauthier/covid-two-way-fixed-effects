**"On the Use of Two-Way Fixed Effects Models for Policy Evaluation During Pandemics", Germain Gauthier (2021)**

**Replication Folder Details**

The repo's structure is as follows:

```
|-- Main Repository
    |-- codes
        |-- 00_main.R
        |-- 01_illustrative_examples.R
        |-- 02_simulations.R
        |-- 03_real_us_data_analysis.R
        |-- simulation_functions.R
    |-- data
        |-- all-states-history.csv
        |-- lockdown_dates.csv
    |-- graphs
```

All scripts are written in R. To ensure replicability, I work with the *renv* package (https://rstudio.github.io/renv/articles/renv.html).

- "00_main.R" runs all scripts. 
- "01_illustrative_examples.R" generates the illustrative examples of the paper.
- "02_simulations.R" runs the deterministic simulations.
- "03_real_us_data_analysis.R" analyzes U.S. data of individuals tested positive for Covid-19 and the timing of implementation of mandatory state lockdowns.
- "simulation_functions.R" provides convenience routines to simulate SIRD models.

The data sources used are:

- "all-states-history.csv" reports the cumulative number of infected and deceased individuals per U.S. State per day due to Covid-19. The data is provided by The Covid Tracking Project (https://Covidtracking.com/data/download).
- "lockdown_dates.csv" provides the periods of time during which U.S. states -- or a subset of a state -- were under lockdown. This is measured by the ``full or partial closure of non-essential retail, ordered by local government'' as reported by Aura Vision (https://auravision.ai/Covid19-lockdown-tracker).

For any further information or comments, drop me a line at: germain.gauthier@polytechnique.edu
